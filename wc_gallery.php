<?php
/**
Plugin Name: Wise Capture Utility Plugin
Plugin URI:  https://bitbucket.org/fusion27/wise-capture-wp-plugin
Description: 1. HelloGlam Flavored Native WordPress gallery handler 2. 'WC Content' custom post type creator 3. 'resue' shortcode creator
Version:     1.3
Author:      Casey Wise
Author URI:  https://caseywise.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: wporg
Domain Path: /languages
*/

add_filter('the_content', 'PrePendWCGallery');
add_action( 'init', 'create_wc_content_post_type' );


function create_wc_content_post_type() {
    register_post_type('wc_content',
        array(
            'labels' => array(
                'name'          => __( 'WC Content' ),
                'singular_name' => __( 'WC Content' )
            ),
            'public'        => true,
            'has_archive'   => true,
        )
    );
}

add_shortcode('reuse', 'create_reusables');
function create_reusables($atts){
    if (array_key_exists("content", $atts)) {
        return GetWCContent($atts["content"]);
    }
}

function GetWCContent($slug) {
    $args = array( 
        'post_type' => 'wc_content', 
        'name'      => $slug
    );
    $loop = new WP_Query( $args );

    while ( $loop->have_posts() ) : $loop->the_post();
        $title = sprintf("<h4>%s</h4>", get_the_title());
        $contentReusable = sprintf("<div class=\"reusable\">%s %s</div>", $title, get_the_content());
    endwhile;

    return $contentReusable;
}

function PrePendWCGallery($content){
    $post = get_post();
    $cat = get_the_category();

    if ($cat[0]->name == "Galleries"){
        echo "<style> pre {border: 1px solid grey; background-color: #dfdfdf; color: #333;} </style>";

        RegsiterAndEnqueueSrciptsAndStyles();
        featured_image_before_content($content);
        echo do_shortcode('[gallery]');
    }

    return $content;
}

function RegsiterAndEnqueueSrciptsAndStyles(){
    wp_register_script('fancybox-script', plugins_url( '/js/fancybox-3.0/dist/jquery.fancybox.min.js', __FILE__ ) );
    wp_enqueue_script('fancybox-script' );
    wp_register_script('wc_gallery', plugins_url( '/js/wc_gallery.js', __FILE__ ) );
    wp_enqueue_script('wc_gallery' );
    wp_register_style('fancybox-style', plugins_url( '/js/fancybox-3.0/dist/jquery.fancybox.min.css', __FILE__ ), array(), '20170527', 'all' );
    wp_enqueue_style('fancybox-style');
}
 
function featured_image_before_content( $content ) { 
    if ( is_singular('post') && has_post_thumbnail()) {
        $thumbnail = get_the_post_thumbnail();
        $content = $thumbnail . $content;
    }
    $featured_image_style = "
    <style type='text/css'>
        #featuredImage {
            margin: auto;
            text-align: center
        }
        #featuredImage img{
            border: 7px solid #dedede;
        }
    </style>\n\t\t";
    echo $featured_image_style;
    echo "<div id=\"featuredImage\">".$content."</div>";
}