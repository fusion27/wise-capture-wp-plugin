# Summary

### Utility plugin to do the following to wisecapture.com WordPress installation

1. HelloGlam Flavored Native WordPress Gallery Handler 
2. `WC Content` custom post type creator 
3. `resue` ShortCode Creator


------

## Gallery Handler
##### Aimed for "as straght-forward as possible" use case.  

To get the sexy looking, responsive gallery:

1. Create a new post.
2. Add media to it 👉 "Insert into Post"
3. Flag the post with the "Galleries" category.
4. Set featured image.
5. Save/Publish

## **WC Content** CPT 

* For our reused content, "what you get back", "prints", "where to shoot", etc.

## **reuse** ShortCode

`[reuse content="prints"]` would pump out the **WC Content** _"prints"_ record.